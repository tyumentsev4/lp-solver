package ru.ac.uniyar.tyumentsev.solver.algo;

import javafx.util.Pair;
import ru.ac.uniyar.tyumentsev.solver.SolverApplication;
import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.FunctionNotRestricted;
import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.SystemIsUnsolvable;
import ru.ac.uniyar.tyumentsev.solver.domain.Fraction;
import ru.ac.uniyar.tyumentsev.solver.domain.Matrix;
import ru.ac.uniyar.tyumentsev.solver.domain.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SimplexMethod {
    private static final Logger logger = Logger.getLogger(SolverApplication.class.getName());
    private ArrayList<Fraction> targetFunction;
    private ArrayList<Integer> basisVarIndexes = new ArrayList<>();
    private final ArrayList<Integer> basisVarNumbers = new ArrayList<>();
    private final ArrayList<Integer> freeVarNumbers = new ArrayList<>();
    private boolean isBasisSelected;
    private final Task task;
    private final ArrayList<SimplexMethodStep> simplexTableHistory = new ArrayList<>();

    /**
     * Конструктор для метода с выбранным базисом
     *
     * @param task                    решаемая задача
     * @param selectedBasisVarIndexes индексы начиная с нуля выбранных базисов
     */
    public SimplexMethod(Task task, ArrayList<Integer> selectedBasisVarIndexes) {
        assert !selectedBasisVarIndexes.isEmpty();
        isBasisSelected = true;
        this.task = task;
        this.targetFunction = task.getTargetFunction().stream().map(el -> {
            try {
                return el.clone();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toCollection(ArrayList::new));
        if (task.getTaskType().equals(Task.TaskType.TO_MAX)) {
            targetFunction = targetFunction.stream().map(el -> Fraction.mul(el, Fraction.MINUS_ONE)).collect(Collectors.toCollection(ArrayList::new));
        }
        this.basisVarIndexes = selectedBasisVarIndexes;
        for (int i = 0; i < task.getnVars(); i++) {
            if (!basisVarIndexes.contains(i)) {
                this.freeVarNumbers.add(i + 1);
            } else {
                this.basisVarNumbers.add(i + 1);
            }
        }
    }

    /**
     * Конструктор для метода искусственного базиса
     *
     * @param task решаемя задача
     */
    public SimplexMethod(Task task) {
        isBasisSelected = false;
        this.task = task;
        this.targetFunction = task.getTargetFunction().stream().map(el -> {
            try {
                return el.clone();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toCollection(ArrayList::new));
        if (task.getTaskType().equals(Task.TaskType.TO_MAX)) {
            targetFunction = targetFunction.stream().map(el -> Fraction.mul(el, Fraction.MINUS_ONE)).collect(Collectors.toCollection(ArrayList::new));
        }
        for (int i = 1; i <= task.getnVars(); i++) {
            freeVarNumbers.add(i);
        }
        for (int i = task.getnVars() + 1; i <= task.getnVars() + task.getnRestr(); i++) {
            basisVarNumbers.add(i);
        }
    }

    /**
     * Выполнить первый шаг симплекс метода
     */
    public void firstStep() throws SystemIsUnsolvable {
        if (simplexTableHistory.isEmpty()) {
            performFirstStep();
        }
    }

    /**
     * Проверить возможен ли следующий шаг симплекс метода
     *
     * @throws FunctionNotRestricted если функция не ограничена.
     */
    public void isNextStep() throws FunctionNotRestricted {
        SimplexMethodStep prevStep = getLastStep();
        if (prevStep.isAuxiliaryTable()) return;
        ArrayList<Pair<Integer, Integer>> allowedRefIndexes = prevStep.getAllowedRefIndexes();
        if (allowedRefIndexes.isEmpty()) {
            throw new FunctionNotRestricted();
        }
    }

    /**
     * Выполнить следующий шаг симплекс метода
     *
     * @param refRow_i индекс опорной строки. Можно установить -1, тогда выбор будет автоматическим.
     * @param refCol_j индекс опорного столбца
     * @throws FunctionNotRestricted если функция не ограничена
     * @throws SystemIsUnsolvable    если вспомогательная задача не может быть решена
     */
    public void nextStep(int refRow_i, int refCol_j) throws FunctionNotRestricted, SystemIsUnsolvable {
        SimplexMethodStep prevStep = getLastStep();
        Matrix newTable = prevStep.getSimplexTable().clone();
        ArrayList<Pair<Integer, Integer>> allowedRefIndexes = prevStep.getAllowedRefIndexes();
        if (isBasisSelected && allowedRefIndexes.isEmpty()) {
            throw new FunctionNotRestricted();
        }
        if (refRow_i == -1 && !prevStep.isAuxiliaryLastTable()) {
            Fraction min = newTable.getElementAt(newTable.getRows() - 1, allowedRefIndexes.get(0).getValue());
            refRow_i = allowedRefIndexes.get(0).getKey();
            refCol_j = allowedRefIndexes.get(0).getValue();
            for (Pair<Integer, Integer> indexes : allowedRefIndexes) {
                if (newTable.getElementAt(newTable.getRows() - 1, indexes.getValue()).compareTo(min) <= 0) {
                    min = newTable.getElementAt(newTable.getRows() - 1, indexes.getValue());
                    refCol_j = indexes.getValue();
                    refRow_i = indexes.getKey();
                }
            }
        }
        SimplexMethodStep simplexMethodStep;
        if (!isBasisSelected) {
            simplexMethodStep = new SimplexMethodStep(newTable, getLastStep(), true);
            if (refRow_i == -1) {
                simplexMethodStep = migrateFromAuxiliaryTable(simplexMethodStep);
                isBasisSelected = true;
                simplexTableHistory.add(simplexMethodStep);
                return;
            }
        } else {
            simplexMethodStep = new SimplexMethodStep(newTable, getLastStep(), false);
        }


        prevStep.setSelectedRefItemCoords(new Pair<>(refRow_i, refCol_j));
        simplexMethodStep.rebuildTable(refRow_i, refCol_j);
        if (!isBasisSelected) {
            try {
                simplexMethodStep.checkAuxiliaryTable();
            } catch (SystemIsUnsolvable e) {
                simplexTableHistory.add(simplexMethodStep);
                throw e;
            }
        }
        simplexTableHistory.add(simplexMethodStep);
    }

    /**
     * Преобразовать таблицу из вспомогательной задачи к основной.
     *
     * @param simplexMethodStep последний шаг вспомогательной задачи
     * @return новый шаг
     */
    private SimplexMethodStep migrateFromAuxiliaryTable(SimplexMethodStep simplexMethodStep) {
        ArrayList<Integer> basisVarNumbers = simplexMethodStep.getBasisVarNumbers();
        ArrayList<Integer> freeVarNumbers = simplexMethodStep.getFreeVarNumbers();
        ArrayList<Fraction> targetFunction = new ArrayList<>(this.targetFunction);
        Matrix simplexTable = simplexMethodStep.getSimplexTable();
        simplexTable.getData().remove(simplexTable.getRows() - 1);
        logger.fine("Recalculating target func\n old value:" + targetFunction);

        // Пересчет целевой функции для основной задачи
        for (int row_i = 0; row_i < simplexTable.getRows(); row_i++) {
            Fraction fr = targetFunction.get(basisVarNumbers.get(row_i) - 1);
            targetFunction.set(basisVarNumbers.get(row_i) - 1, Fraction.ZERO);
            for (int col_j = 0; col_j < simplexTable.getCols() - 1; col_j++) {
                int curFreeIndex = freeVarNumbers.get(col_j) - 1;
                targetFunction.set(curFreeIndex, Fraction.add(targetFunction.get(curFreeIndex), Fraction.mul(fr, Fraction.mul(simplexTable.getElementAt(row_i, col_j), Fraction.MINUS_ONE))));
            }
            targetFunction.set(targetFunction.size() - 1, Fraction.add(Fraction.mul(simplexTable.getElementAt(row_i, simplexTable.getCols() - 1), fr), targetFunction.get(targetFunction.size() - 1)));
        }
        targetFunction.set(targetFunction.size() - 1, Fraction.mul(targetFunction.get(targetFunction.size() - 1), Fraction.MINUS_ONE));
        logger.fine("new value:" + targetFunction);
        ArrayList<Fraction> funcRow = new ArrayList<>();
        for (Integer freeVarNumber : freeVarNumbers) {
            funcRow.add(targetFunction.get(freeVarNumber - 1));
        }
        funcRow.add(targetFunction.get(targetFunction.size() - 1));
        simplexTable.getData().add(funcRow);
        return new SimplexMethodStep(simplexTable, basisVarNumbers, freeVarNumbers);
    }

    /**
     * Выполнить первый шаг
     *
     * @throws SystemIsUnsolvable система не может быть решена
     */
    private void performFirstStep() throws SystemIsUnsolvable {
        // Задача с выбранным базисом
        if (isBasisSelected) {
            // Диагонализируем
            Gauss gauss = new Gauss(task.getRestrictions().clone(), basisVarIndexes);
            gauss.makeDiagonalMatrix();
            Matrix diagMatrix = gauss.getMatrix();
            logger.fine("Diag matrix" + diagMatrix.toString());

            // Пересчитываем целевую функцию
            recalculateTargetFunction(diagMatrix);
            logger.fine("Recalculated target func:" + targetFunction.toString());
            Matrix simplexTable = buildSimplexTable(diagMatrix);
            simplexTableHistory.add(new SimplexMethodStep(simplexTable, basisVarNumbers, freeVarNumbers));
        }
    }

    /**
     * Выполнить первый шаг вспомогательной задачи
     */
    public void performAuxiliaryTableFirstStep() {
        Matrix auxiliaryTable = task.getRestrictions().clone();
        ArrayList<Fraction> func_row = new ArrayList<>(Collections.nCopies(auxiliaryTable.getCols(), Fraction.ZERO));
        for (int col_i = 0; col_i < auxiliaryTable.getCols(); col_i++) {
            for (int row_i = 0; row_i < auxiliaryTable.getRows(); row_i++) {
                func_row.set(col_i, Fraction.add(func_row.get(col_i), auxiliaryTable.getElementAt(row_i, col_i)));
            }
            func_row.set(col_i, Fraction.mul(func_row.get(col_i), Fraction.MINUS_ONE));
        }
        auxiliaryTable.getData().add(func_row);
        simplexTableHistory.add(new SimplexMethodStep(auxiliaryTable, basisVarNumbers, freeVarNumbers, freeVarNumbers.size()));
        logger.fine(auxiliaryTable.toString());
    }

    /**
     * Составить симплекс таблицу из диагональной матрицы
     *
     * @param diagMatrix диагональная матрица
     * @return симплекс таблица
     */
    private Matrix buildSimplexTable(Matrix diagMatrix) {
        ArrayList<ArrayList<Fraction>> table = diagMatrix.clone().getData();
        ArrayList<Fraction> lastRow = new ArrayList<>();
        for (int i = 0; i < targetFunction.size() - 1; i++) {
            lastRow.add(targetFunction.get(i));
        }
        lastRow.add(Fraction.mul(targetFunction.get(targetFunction.size() - 1), Fraction.MINUS_ONE));
        table.add(lastRow);

        for (int i = task.getnVars() - 1; i >= 0; i--) {
            if (basisVarIndexes.contains(i)) {
                for (ArrayList<Fraction> fractions : table) {
                    fractions.remove(i);
                }
            }
        }
        return new Matrix(table);
    }

    /**
     * Пересчитывает целевую функцию
     *
     * @param diagMatrix диагональная матрица
     */
    private void recalculateTargetFunction(Matrix diagMatrix) {
        for (int i = 0; i < diagMatrix.getRows(); i++) {
            Integer basisVarIndex = basisVarIndexes.get(i);
            ArrayList<Fraction> temp_func = new ArrayList<>();
            for (int j = 0; j < diagMatrix.getCols(); j++) {
                if (basisVarIndexes.contains(j)) {
                    temp_func.add(new Fraction(0, 1));
                } else {
                    if (j == diagMatrix.getCols() - 1) {
                        temp_func.add(diagMatrix.getElementAt(i, j));
                    }
                    else {
                        temp_func.add(Fraction.mul(diagMatrix.getElementAt(i, j), Fraction.MINUS_ONE));
                    }
                }
            }
            Fraction basisFactor = targetFunction.get(basisVarIndex);
            for (int j = 0; j < temp_func.size(); j++) {
                targetFunction.set(j, Fraction.add(targetFunction.get(j), Fraction.mul(temp_func.get(j), basisFactor)));
            }
            targetFunction.set(basisVarIndex, new Fraction(0, 1));
        }
    }

    public ArrayList<SimplexMethodStep> getSimplexTableHistory() {
        return simplexTableHistory;
    }

    public boolean isSolved() {
        return !simplexTableHistory.isEmpty() && getLastStep().isSolved() && !getLastStep().isAuxiliaryLastTable();
    }

    public void markSolved() {
        getLastStep().markSolved();
    }

    /**
     * Получить ответ для решенной таблице
     *
     * @return массив со значениями переменных, на последнем месте значение функции
     */
    private ArrayList<Fraction> getAnswer() {
        if (simplexTableHistory.isEmpty()) return null;
        ArrayList<Fraction> ans = getLastStep().getAnswer();
        if (task.getTaskType() == Task.TaskType.TO_MAX) {
            ans.set(ans.size() - 1, Fraction.mul(ans.get(ans.size() - 1), Fraction.MINUS_ONE));
        }
        return ans;
    }

    /**
     * Получить последний шаг симплекс метода
     */
    public SimplexMethodStep getLastStep() {
        if (simplexTableHistory.isEmpty()) return null;
        return simplexTableHistory.get(simplexTableHistory.size() - 1);
    }

    /**
     * Сделать шаг назад
     */
    public void undoStep() {
        if (!simplexTableHistory.isEmpty()) {
            simplexTableHistory.remove(simplexTableHistory.size() - 1);
            if (getLastStep() != null && getLastStep().isAuxiliaryLastTable()) {
                isBasisSelected = false;
            }
        }
    }

    /**
     * Получить ответ в виде строки
     */
    public String getAnswerStr() {
        ArrayList<Fraction> answer = getAnswer();
        StringBuilder ansStr = new StringBuilder("f(");
        for (int i = 0; i < Objects.requireNonNull(answer).size() - 1; i++) {
            ansStr.append(answer.get(i));
            if (i == answer.size() - 2) {
                ansStr.append(")");
            } else {
                ansStr.append(",");
            }
        }
        ansStr.append(" = ").append(answer.get(answer.size() - 1));
        return ansStr.toString();
    }
}
