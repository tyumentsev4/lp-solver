module ru.ac.uniyar.tyumentsev.solver {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.databind;
    requires java.logging;


    opens ru.ac.uniyar.tyumentsev.solver to javafx.fxml;
    exports ru.ac.uniyar.tyumentsev.solver;
    exports ru.ac.uniyar.tyumentsev.solver.controllers;
    exports ru.ac.uniyar.tyumentsev.solver.algo;
    exports ru.ac.uniyar.tyumentsev.solver.algo.exceptions;
    opens ru.ac.uniyar.tyumentsev.solver.controllers to javafx.fxml;
    exports ru.ac.uniyar.tyumentsev.solver.domain to com.fasterxml.jackson.databind;
    opens ru.ac.uniyar.tyumentsev.solver.domain to com.fasterxml.jackson.databind;
}
