package ru.ac.uniyar.tyumentsev.solver.controllers;

import javafx.fxml.FXML;
import javafx.geometry.NodeOrientation;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.util.Pair;
import ru.ac.uniyar.tyumentsev.solver.SolverApplication;
import ru.ac.uniyar.tyumentsev.solver.algo.Gauss;
import ru.ac.uniyar.tyumentsev.solver.algo.SimplexMethod;
import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.FunctionNotRestricted;
import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.SystemIsUnsolvable;
import ru.ac.uniyar.tyumentsev.solver.domain.Fraction;
import ru.ac.uniyar.tyumentsev.solver.domain.Graph;
import ru.ac.uniyar.tyumentsev.solver.domain.Matrix;
import ru.ac.uniyar.tyumentsev.solver.domain.Task;
import ru.ac.uniyar.tyumentsev.solver.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GraphicController {

    public NumberAxis yAx;
    public NumberAxis xAx;
    protected Task task;

    private final ArrayList<Integer> basisIndexes = new ArrayList<>();
    private final ArrayList<Integer> freeIndexes = new ArrayList<>();

    private ArrayList<Fraction> func;
    private ArrayList<ArrayList<Fraction>> lines;
    private ArrayList<Pair<Fraction, Fraction>> apexes;
    private boolean isDrawn = false;
    private Pair<Fraction, Fraction> endPoint;

    @FXML
    private LineChart<Double, Double> lineChart;

    @FXML
    private HBox checkBasisLine;

    @FXML
    private Label answer;

    private Graph mathsGraph;

    protected static final Logger logger = Logger.getLogger(SolverApplication.class.getName());

    public void setTask(Task task) {
        this.task = task;
        task.addListener(this::onTaskUpdate);
        onTaskUpdate();
    }

    public void onTaskUpdate() {
        createBasisChooseCheckBoxes();
        clear();
    }

    private void plotLine(Function<Double, Double> function, String name) {
        mathsGraph.plotLine(function, name);
    }

    private void plotApexes(ArrayList<Pair<Fraction, Fraction>> apexes) {
        XYChart.Series<Double, Double> series = new XYChart.Series<>();
        for (var point : apexes) {
            series.getData().add(new XYChart.Data<>(point.getKey().toDouble(), point.getValue().toDouble()));
        }
        series.setName("Вершины");
        lineChart.getData().add(series);
    }

    private void clear() {
        isDrawn = false;
        lineChart.getData().clear();
        func = new ArrayList<>();
        lines = new ArrayList<>();
        apexes = new ArrayList<>();
        answer.setText("");
    }

    private void setBasisCheckboxesDisable() {
        for (Node node : checkBasisLine.getChildren()) {
            node.setDisable(true);
        }
    }

    private void createBasisChooseCheckBoxes() {
        checkBasisLine.getChildren().clear();
        checkBasisLine.getChildren().add(new Label("Базисные переменные: "));
        for (int i = 1; i < task.getnVars() + 1; i++) {
            CheckBox checkBox = new CheckBox("x_" + i);
            checkBox.setOnAction((event) -> onBasisChange());
            checkBox.setId(String.valueOf(i - 1));
            checkBox.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
            if (i <= task.getnRestr()) {
                checkBox.setSelected(true);
            }
            checkBasisLine.getChildren().add(checkBox);
        }
        onBasisChange();
    }

    private long getSelectedCountCheckbox() {
        return checkBasisLine.getChildren().stream().skip(1).filter((node -> ((CheckBox) node).isSelected())).count();
    }

    private void onBasisChange() {
        basisIndexes.clear();
        freeIndexes.clear();
        for (Node node : checkBasisLine.getChildren()) {
            node.setDisable(false);
        }
        if (task.getnRestr() == getSelectedCountCheckbox()) {
            for (Node node : checkBasisLine.getChildren()) {
                if (!(node instanceof CheckBox)) continue;
                if (!((CheckBox) node).isSelected()) {
                    node.setDisable(true);
                } else {
                    basisIndexes.add(Integer.parseInt(node.getId()));
                }
            }
            for (int i = 0; i < task.getnVars(); i++) {
                if (!basisIndexes.contains(i)) {
                    freeIndexes.add(i);
                }
            }
            if (freeIndexes.size() == 2) {
                lineChart.getXAxis().setLabel("x" + (freeIndexes.get(0) + 1));
                lineChart.getYAxis().setLabel("x" + (freeIndexes.get(1) + 1));
            }
        }
    }

    private boolean basisNotSelected() {
        if (basisIndexes.size() != task.getnRestr()) {
            logger.log(Level.WARNING, "Не выбраны базисные переменные");
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("Не выбраны базисные переменные");
            a.setContentText("Выберите базисные переменные");
            a.show();
            return true;
        }
        return false;
    }

    @FXML
    void onGetAnswer() {
        if (isDrawn) return;
        clear();
        if (basisNotSelected()) return;
        setBasisCheckboxesDisable();
        if (task.getnVars() - task.getnRestr() > 2) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("Графический метод");
            a.setContentText("Полученная система имеет более 2х свободных переменных");
            a.show();
            onReset();
            return;
        }
        if (task.getnVars() - task.getnRestr() < 2) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("Графический метод");
            a.setContentText("Полученная система имеет менее 2х свободных переменных. Вы можете добавить нулевой столбец.");
            a.show();
            onReset();
            return;
        }
        try {
            calculateLines();

        } catch (SystemIsUnsolvable e) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("Метод Гаусса: ошибка");
            a.setContentText("Полученная система не разрешима, выберите другие базисные переменные");
            a.show();
            onReset();
            return;
        } catch (FunctionNotRestricted e) {
            answer.setText("Функция не ограничена снизу");
        }
        drawGraphic();
    }

    private void drawGraphic() {
        var xAxis = new ArrayList<>(List.of(Fraction.MINUS_ONE, Fraction.ZERO, Fraction.ZERO));
        var yAxis = new ArrayList<>(List.of(Fraction.ZERO, Fraction.MINUS_ONE, Fraction.ZERO));
        lines.add(xAxis);
        lines.add(yAxis);
        var points = findPoints();
        findApexes(points);
        double maxX = 5.0;
        double maxY = 5.0;
        Pair<Double, Double> maxApex = new Pair<>(maxX, maxY);
        for (Pair<Fraction, Fraction> apex : apexes) {
            if (apex.getKey().toDouble() >= maxX || (apex.getKey().toDouble() == maxX && apex.getValue().toDouble() >= maxY)) {
                maxX = apex.getKey().toDouble();
                maxY = apex.getValue().toDouble();
                maxApex = new Pair<>(maxX+1, maxY+1);
            }
        }

        xAx.upperBoundProperty().set(maxApex.getKey());
        yAx.upperBoundProperty().set(maxApex.getValue());
        mathsGraph = new Graph(lineChart, maxApex);

        plotApexes(apexes);
        plotAntiNormal();
        lines.remove(xAxis);
        lines.remove(yAxis);

        for (int i = 0; i < lines.size(); i++) {
            var a = lines.get(i).get(0).toDouble();
            var b = lines.get(i).get(1).toDouble();
            var c = lines.get(i).get(2).toDouble();
            String name = "x" + StringUtils.subscript(basisIndexes.get(i) + 1) + " = " + strFunction(lines.get(i));
            if (b == 0) {
                mathsGraph.plotVerticalSeries(c / a, name);
            } else {
                plotLine(x -> (a * x - c) / -b, name);
            }
        }
        isDrawn = true;
    }

    private String strFunction(ArrayList<Fraction> func) {
        StringBuilder sb = new StringBuilder();
        var a = Fraction.mul(func.get(0), Fraction.MINUS_ONE);
        if (a.compareTo(Fraction.ZERO) != 0) {
            if (a.compareTo(Fraction.ONE) == 0) {
                sb.append("x").append(StringUtils.subscript(freeIndexes.get(0) + 1));
            } else if (a.compareTo(Fraction.MINUS_ONE) == 0) {
                sb.append("-x").append(StringUtils.subscript(freeIndexes.get(0) + 1));
            } else {
                sb.append(a).append("x").append(StringUtils.subscript(freeIndexes.get(0) + 1));
            }
        }
        var b = Fraction.mul(func.get(1), Fraction.MINUS_ONE);
        if (b.compareTo(Fraction.ZERO) != 0) {
            if (sb.length() > 0) {
                sb.append(b.compareTo(Fraction.ZERO) > 0 ? " + " : " - ");
            }
            if (Fraction.abs(b).compareTo(Fraction.ONE) == 0) {
                sb.append("x").append(StringUtils.subscript(freeIndexes.get(1) + 1));
            } else {
                sb.append(Fraction.abs(b)).append("x").append(StringUtils.subscript(freeIndexes.get(1) + 1));
            }
        }
        var c = func.get(2);
        if (c.compareTo(Fraction.ZERO) != 0) {
            if (sb.length() > 0) {
                sb.append(c.compareTo(Fraction.ZERO) > 0 ? " + " : " - ");
            }
            sb.append(Fraction.abs(c));
        }
        if (sb.length() == 0) {
            sb.append("0");
        }
        return sb.toString();
    }

    private void plotAntiNormal() {
        final XYChart.Series<Double, Double> series = new XYChart.Series<>();
        series.getData().add(new XYChart.Data<>(endPoint.getKey().toDouble(), endPoint.getValue().toDouble()));
        var dx = func.get(0).toDouble() * -1;
        var dy = func.get(1).toDouble() * -1;
        var len = Math.sqrt(dx * dx + dy * dy);
        series.getData().add(new XYChart.Data<>(dx / len + endPoint.getKey().toDouble(), dy / len + endPoint.getValue().toDouble()));
        if (task.getTaskType() == Task.TaskType.TO_MIN)
            series.setName("Антинормаль");
        else
            series.setName("Нормаль");
        lineChart.getData().add(series);
    }

    private void findApexes(ArrayList<Pair<Fraction, Fraction>> points) {
        apexes = new ArrayList<>();
        for (var point : points) {
            boolean isApex = true;
            for (var line : lines) {
                if (Fraction.add(Fraction.mul(line.get(0), point.getKey()), Fraction.mul(line.get(1), point.getValue())).compareTo(line.get(2)) > 0) {
                    isApex = false;
                }
            }
            if (isApex) {
                apexes.add(point);
            }
        }
    }

    private ArrayList<Pair<Fraction, Fraction>> findPoints() {
        ArrayList<Pair<Fraction, Fraction>> points = new ArrayList<>();
        for (int i = 0; i < lines.size() - 1; i++) {
            for (int j = i + 1; j < lines.size(); j++) {
                try {
                    var point = findPoint(lines.get(i), lines.get(j));
                    if (point.getKey().compareTo(Fraction.ZERO) < 0 || point.getValue().compareTo(Fraction.ZERO) < 0)
                        continue;
                    points.add(point);
                } catch (SystemIsUnsolvable ignored) {
                }
            }
        }
        return points;
    }

    private Pair<Fraction, Fraction> findPoint(ArrayList<Fraction> lineA, ArrayList<Fraction> lineB) throws SystemIsUnsolvable {
        ArrayList<ArrayList<Fraction>> data = new ArrayList<>();
        data.add(lineA);
        data.add(lineB);
        var gauss = new Gauss(new Matrix(data).clone(), new ArrayList<>(List.of(0, 1)));
        gauss.makeDiagonalMatrix();
        var table = gauss.getMatrix();
        return new Pair<>(table.getElementAt(0, table.getCols()-1), table.getElementAt(1, table.getCols()-1));
    }

    private void calculateLines() throws SystemIsUnsolvable, FunctionNotRestricted {
        var sm = new SimplexMethod(task, basisIndexes);
        sm.firstStep();
        var step = sm.getLastStep();
        var table = step.getSimplexTable().clone();

        for (int i = 0; i < table.getRows() - 1; i++) {
            ArrayList<Fraction> line = new ArrayList<>();
            for (int j = 0; j < table.getCols(); j++) {
                line.add(table.getElementAt(i, j));
            }
            lines.add(line);
        }
        for (int j = 0; j < table.getCols() - 1; j++) {
            func.add(table.getElementAt(table.getRows() - 1, j));
        }
        func.add(Fraction.mul(table.getElementAt(table.getRows() - 1, table.getCols() - 1), Fraction.MINUS_ONE));
        while (!sm.isSolved()) {
            sm.nextStep(-1, -1);
        }
        step = sm.getLastStep();
        var ans = step.getAnswer();
        endPoint = new Pair<>(ans.get(freeIndexes.get(0)), ans.get(freeIndexes.get(1)));

        answer.setText("Ответ: " + sm.getAnswerStr());
    }

    @FXML
    void onReset() {
        onBasisChange();
        clear();
    }
}
