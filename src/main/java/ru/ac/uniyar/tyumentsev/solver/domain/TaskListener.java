package ru.ac.uniyar.tyumentsev.solver.domain;

public interface TaskListener {
    void onTaskUpdate();
}
