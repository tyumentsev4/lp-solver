package ru.ac.uniyar.tyumentsev.solver.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ru.ac.uniyar.tyumentsev.solver.SolverApplication;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class Task {
    private static final Logger logger = Logger.getLogger(SolverApplication.class.getName());
    Integer DEFAULT_N_VARS = 4;
    Integer DEFAULT_N_RESTR = 2;

    private TaskType taskType;
    private FractionType fractionType;

    private ArrayList<Fraction> targetFunction;
    private Matrix restrictions;

    private Integer nRestr;
    private Integer nVars;

    public Task() {
        reset();
    }

    public void fillTask(Integer nRestr, Integer nVars) {
        this.nRestr = nRestr;
        this.nVars = nVars;
        this.targetFunction = new ArrayList<>(Collections.nCopies(nVars + 1, new Fraction()));
        this.restrictions = Matrix.zeroMatrix(nRestr, nVars + 1);
    }

    public void reset() {
        this.taskType = TaskType.TO_MIN;
        this.fractionType = FractionType.COMMON;
        fillTask(DEFAULT_N_RESTR, DEFAULT_N_VARS);
        for (TaskListener listener : listeners) {
            listener.onTaskUpdate();
        }
    }

    public enum TaskType {
        TO_MIN("На минимум"),
        TO_MAX("На максимум");

        private final String label;

        TaskType(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }

    public enum FractionType {
        COMMON("Обыкновенные"),
        DECIMAL("Десятичные");

        private final String label;

        FractionType(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }

    public Integer getnRestr() {
        return nRestr;
    }

    public Integer getnVars() {
        return nVars;
    }

    public void setnRestr(Integer nRestr) {
        this.nRestr = nRestr;
    }

    public void setnVars(Integer nVars) {
        this.nVars = nVars;
    }

    public Matrix getRestrictions() {
        return restrictions;
    }

    public ArrayList<Fraction> getTargetFunction() {
        return targetFunction;
    }

    public FractionType getFractionType() {
        return fractionType;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTargetFunction(ArrayList<Fraction> targetFunction) {
        this.targetFunction = targetFunction;
    }

    public void setFractionType(FractionType fractionType) {
        this.fractionType = fractionType;
        Fraction.fractionType = fractionType;
    }

    public void setRestrictions(Matrix restrictions) {
        this.restrictions = restrictions;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    private final List<TaskListener> listeners = new ArrayList<>();

    public void addListener(TaskListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(TaskListener listener) {
        listeners.remove(listener);
    }

    public void updateTask(ArrayList<Fraction> targetFunction, Matrix restrictions, TaskType taskType, FractionType fractionType) {
        this.targetFunction = targetFunction;
        this.restrictions = restrictions;
        this.taskType = taskType;
        this.fractionType = fractionType;

        logger.info("Task updated");
        logger.fine("Target function: " + targetFunction.toString());
        logger.fine("Task type: " + taskType);
        logger.fine("Fraction type: " + fractionType);
        logger.fine("Restrictions: " + restrictions.toString());

        for (TaskListener listener : listeners) {
            listener.onTaskUpdate();
        }
    }

    public void saveJSON(File file) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            objectMapper.writeValue(file, this);
            logger.info("Task saved to file.");
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    public static Task readJSON(File file) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Task task = objectMapper.readValue(file, Task.class);
            logger.info("Task loaded from file.");
            return task;
        } catch (IOException e) {
            throw new IOException(e);
        }
    }
}
