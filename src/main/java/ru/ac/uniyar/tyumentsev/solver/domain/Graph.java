package ru.ac.uniyar.tyumentsev.solver.domain;

import javafx.scene.chart.XYChart;
import javafx.util.Pair;

import java.util.function.Function;

public class Graph {
    private final XYChart<Double, Double> graph;
    private final Pair<Double, Double> range;

    public Graph(final XYChart<Double, Double> graph, final Pair<Double, Double> range) {
        this.graph = graph;
        this.range = range;
    }

    public void plotLine(final Function<Double, Double> function, String name) {
        final XYChart.Series<Double, Double> series = new XYChart.Series<>();
        for (double x = -range.getKey(); x <= range.getKey(); x = x + 0.01) {
            plotPoint(x, function.apply(x), series);
        }
        series.setName(name);
        graph.getData().add(series);
    }

    public void plotVerticalSeries(Double x, String name) {
        final XYChart.Series<Double, Double> series = new XYChart.Series<>();
        for (double y = -range.getValue(); y <= range.getValue(); y = y + 0.01) {
            plotPoint(x, y, series);
        }
        series.setName(name);
        graph.getData().add(series);
    }

    private void plotPoint(final double x, final double y,
                           final XYChart.Series<Double, Double> series) {
        series.getData().add(new XYChart.Data<>(x, y));
    }

}
