package ru.ac.uniyar.tyumentsev.solver.controllers;

import javafx.fxml.FXML;
import javafx.geometry.NodeOrientation;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import ru.ac.uniyar.tyumentsev.solver.algo.SimplexMethod;
import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.SystemIsUnsolvable;

import java.util.ArrayList;
import java.util.logging.Level;

public class SelectedBasisController extends SimplexMethodController {
    @FXML
    private HBox checkBasisLine;
    private final ArrayList<Integer> basisIndexes = new ArrayList<>();

    public void onTaskUpdate() {
        super.onTaskUpdate();
        createBasisChooseCheckBoxes();
        createSimplexTable(task.getnVars() - task.getnRestr() + 2);
    }

    @FXML
    void onGetAnswer() {
        makeFirstStep();
        super.onGetAnswer();
    }

    private boolean makeFirstStep() {
        if (basisNotSelected()) return true;
        if (getnStep() == 0) {
            setBasisCheckboxesDisable(true);
            sm = new SimplexMethod(task, basisIndexes);
            try {
                sm.firstStep();
            } catch (SystemIsUnsolvable e) {
                Alert a = new Alert(Alert.AlertType.WARNING);
                a.setTitle("Метод Гаусса: ошибка");
                a.setContentText("Полученная система не разрешима, выберите другие базисные переменные");
                a.show();
                onStepBack();
            }
            return true;
        }
        return false;
    }

    @FXML
    void onStepForward() {
        if (makeFirstStep()) {
            drawDataInTable();
            return;
        }
        super.onStepForward();
    }

    @FXML
    void onStepBack() {
        super.onStepBack();
        if (getnStep() == 0) {
            setBasisCheckboxesDisable(false);
            onBasisChange();
        }
    }

    private void setBasisCheckboxesDisable(boolean state) {
        for (Node node : checkBasisLine.getChildren()) {
            node.setDisable(state);
        }
    }

    private void createBasisChooseCheckBoxes() {
        checkBasisLine.getChildren().clear();
        checkBasisLine.getChildren().add(new Label("Базисные переменные: "));
        for (int i = 1; i < task.getnVars() + 1; i++) {
            CheckBox checkBox = new CheckBox("x_" + i);
            checkBox.setOnAction((event) -> onBasisChange());
            checkBox.setId(String.valueOf(i - 1));
            checkBox.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
            if (i <= task.getnRestr()) {
                checkBox.setSelected(true);
            }
            checkBasisLine.getChildren().add(checkBox);
        }
        onBasisChange();
    }

    private long getSelectedCountCheckbox() {
        return checkBasisLine.getChildren().stream().skip(1).filter((node -> ((CheckBox) node).isSelected())).count();
    }

    private void onBasisChange() {
        basisIndexes.clear();
        for (Node node : checkBasisLine.getChildren()) {
            node.setDisable(false);
        }
        if (task.getnRestr() == getSelectedCountCheckbox()) {
            for (Node node : checkBasisLine.getChildren()) {
                if (!(node instanceof CheckBox)) continue;
                if (!((CheckBox) node).isSelected()) {
                    node.setDisable(true);
                } else {
                    basisIndexes.add(Integer.parseInt(node.getId()));
                }
            }
        }
    }

    private boolean basisNotSelected() {
        if (basisIndexes.size() != task.getnRestr()) {
            logger.log(Level.WARNING, "Не выбраны базисные переменные");
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("Не выбраны базисные переменные");
            a.setContentText("Выберите базисные переменные");
            a.show();
            return true;
        }
        return false;
    }
}
