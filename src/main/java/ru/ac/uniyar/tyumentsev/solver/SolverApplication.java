package ru.ac.uniyar.tyumentsev.solver;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SolverApplication extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(SolverApplication.class.getResource("menu-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 840, 610);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("styles/style.css")).toExternalForm());
        stage.setTitle("Решение задачи линейного программирования");
        stage.getIcons().add(new Image(Objects.requireNonNull(SolverApplication.class.getResourceAsStream("icon.png"))));
        stage.setScene(scene);
        stage.setMinHeight(620);
        stage.setMinWidth(840);
        stage.show();
    }

    public static void main(String[] args) {
        Logger logger = Logger.getLogger(SolverApplication.class.getName());
        if (Objects.equals(System.getenv().get("DEBUG"), "TRUE")) {
            logger.setUseParentHandlers(false);
            logger.setLevel(Level.FINE);
            ConsoleHandler handler = new ConsoleHandler();
            handler.setLevel(Level.FINER);
            logger.addHandler(handler);
            logger.fine("Debug mode on.");
        }

        logger.info("Start application...");
        launch();
    }
}
