package ru.ac.uniyar.tyumentsev.solver.algo;

import javafx.util.Pair;
import ru.ac.uniyar.tyumentsev.solver.SolverApplication;
import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.SystemIsUnsolvable;
import ru.ac.uniyar.tyumentsev.solver.domain.Fraction;
import ru.ac.uniyar.tyumentsev.solver.domain.Matrix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimplexMethodStep {
    private static final Logger logger = Logger.getLogger(SolverApplication.class.getName());
    private final ArrayList<Integer> basisVarNumbers;
    private final ArrayList<Integer> freeVarNumbers;
    private final Matrix simplexTable;
    private final int origVarsNum;
    private Pair<Integer, Integer> selectedRefItemCoords;
    private boolean isSolved = false;
    private boolean isAuxiliaryTable = false;

    public SimplexMethodStep(Matrix simplexTable, ArrayList<Integer> basisVarNumbers, ArrayList<Integer> freeVarNumbers) {
        this.simplexTable = simplexTable;
        this.basisVarNumbers = basisVarNumbers;
        this.freeVarNumbers = freeVarNumbers;
        this.origVarsNum = basisVarNumbers.size() + freeVarNumbers.size();
    }

    public SimplexMethodStep(Matrix simplexTable, ArrayList<Integer> basisVarNumbers, ArrayList<Integer> freeVarNumbers, int origVarsNum) {
        this.simplexTable = simplexTable;
        this.basisVarNumbers = basisVarNumbers;
        this.freeVarNumbers = freeVarNumbers;
        this.origVarsNum = origVarsNum;
        isAuxiliaryTable = true;
    }

    public boolean isAuxiliaryTable() {
        return isAuxiliaryTable;
    }

    public SimplexMethodStep(Matrix simplexTable, SimplexMethodStep prevStep, boolean isAuxiliaryTable) throws SystemIsUnsolvable {
        this.simplexTable = simplexTable;
        this.basisVarNumbers = new ArrayList<>(prevStep.getBasisVarNumbers());
        this.freeVarNumbers = new ArrayList<>(prevStep.getFreeVarNumbers());
        this.origVarsNum = prevStep.origVarsNum;
        if (isAuxiliaryTable) {
            this.isAuxiliaryTable = checkAuxiliaryTable();
        } else {
            this.isAuxiliaryTable = false;
        }
    }

    public Matrix getSimplexTable() {
        return simplexTable;
    }

    public ArrayList<Integer> getBasisVarNumbers() {
        return basisVarNumbers;
    }

    public ArrayList<Integer> getFreeVarNumbers() {
        return freeVarNumbers;
    }

    /**
     * Проверка решена ли задача.
     * */
    public boolean isSolved() {
        if (isSolved) return true;
        for (int i = 0; i < simplexTable.getCols() - 1; i++) {
            if (simplexTable.getElementAt(simplexTable.getRows() - 1, i).compareTo(new Fraction(0, 1)) < 0) {
                return false;
            }
        }
        isSolved = true;
        return true;
    }

    /**
     * Является ли эта таблица вспомогательной
     * */
    public boolean checkAuxiliaryTable() throws SystemIsUnsolvable {
        if (!isAuxiliaryTable) return true;
        if (isSolved()) {
            Fraction fValue = simplexTable.getElementAt(simplexTable.getRows() - 1, simplexTable.getCols() - 1);
            if (fValue.equals(Fraction.ZERO)) return false;
            if (fValue.compareTo(Fraction.ZERO) < 0) throw new SystemIsUnsolvable();
            isAuxiliaryTable = false;
        }
        return true;
    }

    public boolean isAuxiliaryLastTable() {
        return isAuxiliaryTable && isSolved() && simplexTable.getElementAt(simplexTable.getRows() - 1, simplexTable.getCols() - 1).equals(Fraction.ZERO);
    }

    /**
     * Считает ответ для решенной таблице
     *
     * @return массив со значениями переменных, на последнем месте значение функции
     */
    public ArrayList<Fraction> getAnswer() {
        if (!isSolved()) return null;
        ArrayList<Fraction> ans = new ArrayList<>(Collections.nCopies(basisVarNumbers.size() + freeVarNumbers.size(), Fraction.ZERO));
        int i = 0;
        for (int basisVarNum : basisVarNumbers) {
            ans.set(basisVarNum - 1, simplexTable.getElementAt(i, simplexTable.getCols() - 1));
            i++;
        }
        ans.add(Fraction.mul(simplexTable.getElementAt(simplexTable.getRows() - 1, simplexTable.getCols() - 1), Fraction.MINUS_ONE));
        return ans;
    }

    /**
     * Строит новую таблицу для следующего шага симплекс метода
     * @param refRow_i индекс опорной строки.
     * @param refCol_j индекс опорного столбца
     * */
    public void rebuildTable(int refRow_i, int refCol_j) {
        // 1 шаг. Поменять местами опорные переменные
        int tmp = basisVarNumbers.get(refRow_i);
        basisVarNumbers.set(refRow_i, freeVarNumbers.get(refCol_j));
        freeVarNumbers.set(refCol_j, tmp);

        // 2 шаг. На место опорного элемента поставить 1/опорный элемент
        simplexTable.setElementAt(refRow_i, refCol_j,
            Fraction.div(Fraction.ONE, simplexTable.getElementAt(refRow_i, refCol_j)));

        // 3 шаг. Разделить строку на опорный элемент
        for (int col_j = 0; col_j < getSimplexTable().getCols(); col_j++) {
            if (col_j != refCol_j) {
                simplexTable.setElementAt(refRow_i, col_j,
                    Fraction.mul(simplexTable.getElementAt(refRow_i, col_j), simplexTable.getElementAt(refRow_i, refCol_j)));
            }
        }

        // 4 шаг. Разделить колонку на -1 * опорный элемент
        for (int row_i = 0; row_i < getSimplexTable().getRows(); row_i++) {
            if (row_i != refRow_i) {
                simplexTable.setElementAt(row_i, refCol_j,
                    Fraction.mul(simplexTable.getElementAt(row_i, refCol_j),
                        Fraction.mul(Fraction.MINUS_ONE, simplexTable.getElementAt(refRow_i, refCol_j))));
            }
        }

        // 5 шаг. Вычитание опорной строки из остальных элементов
        for (int row_i = 0; row_i < simplexTable.getRows(); row_i++) {
            if (row_i == refRow_i) continue;
            for (int col_j = 0; col_j < simplexTable.getCols(); col_j++) {
                if (col_j == refCol_j) continue;
                simplexTable.setElementAt(row_i, col_j, Fraction.sub(simplexTable.getElementAt(row_i, col_j), Fraction.mul(Fraction.mul(Fraction.MINUS_ONE, Fraction.div(simplexTable.getElementAt(row_i, refCol_j), simplexTable.getElementAt(refRow_i, refCol_j))), simplexTable.getElementAt(refRow_i, col_j))));
            }
        }

        // Если решаем вспомогательную задачу, удаляем опорный столбец, если он соответствует вспомогательной переменной
        if (this.isAuxiliaryTable) {
            if (freeVarNumbers.get(refCol_j) > origVarsNum) {
                for (int row_i = 0; row_i < simplexTable.getRows(); row_i++) {
                    simplexTable.getData().get(row_i).remove(refCol_j);
                }
                freeVarNumbers.remove(refCol_j);
            }
            // удаление нулевых строк
            ArrayList<Integer> rows_i = new ArrayList<>();
            for (int row_i = 0; row_i < simplexTable.getRows()-1; row_i++) {
                if (basisVarNumbers.get(row_i) > origVarsNum) {
                    if (simplexTable.getData().get(row_i).stream().allMatch((el) -> el.equals(Fraction.ZERO))) {
                        rows_i.add(row_i);
                    }
                }
            }
            for (Integer row_i: rows_i) {
                simplexTable.getData().remove((int) row_i);
                basisVarNumbers.remove((int) row_i);
            }
        }

        logger.fine(simplexTable.toString());
    }

    /**
     * Получить координаты доступных для выбора опорных элементов.
     * */
    public ArrayList<Pair<Integer, Integer>> getAllowedRefIndexes() {
        ArrayList<Pair<Integer, Integer>> res = new ArrayList<>();
        Matrix table = this.getSimplexTable();

        for (int col_i = 0; col_i < table.getCols() - 1; col_i++) {
            if (table.getElementAt(table.getRows() - 1, col_i).compareTo(Fraction.ZERO) < 0) {
                int cur_row_i = -1;
                ArrayList<Integer> rows_i = new ArrayList<>();
                for (int row_i = 0; row_i < table.getRows() - 1; row_i++) {
                    if (table.getElementAt(row_i, col_i).compareTo(Fraction.ZERO) <= 0) {
                        continue;
                    }
                    if (cur_row_i == -1) {
                        cur_row_i = row_i;
                        rows_i.add(row_i);
                        continue;
                    }
                    if (Fraction.div(table.getElementAt(row_i, table.getCols() - 1), table.getElementAt(row_i, col_i))
                        .compareTo(Fraction.div(table.getElementAt(cur_row_i, table.getCols() - 1), table.getElementAt(cur_row_i, col_i))) == 0) {
                        cur_row_i = row_i;
                        rows_i.add(row_i);
                    }
                    if (Fraction.div(table.getElementAt(row_i, table.getCols() - 1), table.getElementAt(row_i, col_i))
                        .compareTo(Fraction.div(table.getElementAt(cur_row_i, table.getCols() - 1), table.getElementAt(cur_row_i, col_i))) < 0) {
                        cur_row_i = row_i;
                        rows_i.clear();
                        rows_i.add(row_i);
                    }
                }
                if (cur_row_i != -1) {
                    for (Integer row_i: rows_i) {
                        res.add(new Pair<>(row_i, col_i));
                    }
                }
            }
        }
        logger.log(Level.FINE, "allowed ref coordinates: " + res);
        return res;
    }

    public void setSelectedRefItemCoords(Pair<Integer, Integer> selectedRefItemCoords) {
        this.selectedRefItemCoords = selectedRefItemCoords;
    }

    public Pair<Integer, Integer> getSelectedRefItemCoords() {
        return selectedRefItemCoords;
    }

    public void markSolved() {
        isSolved = true;
    }
}
