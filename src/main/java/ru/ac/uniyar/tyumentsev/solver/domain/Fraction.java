package ru.ac.uniyar.tyumentsev.solver.domain;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;
import java.text.DecimalFormat;

public class Fraction implements Cloneable, Comparable<Fraction> {
    @JsonProperty
    public BigInteger numerator;
    @JsonProperty
    public BigInteger denominator;
    public static Task.FractionType fractionType = Task.FractionType.COMMON;

    public Fraction() {
        super();
        numerator = BigInteger.valueOf(0);
        denominator = BigInteger.valueOf(1);
    }

    public Fraction(BigInteger numerator, BigInteger denominator) {
        if (denominator.equals(BigInteger.ZERO)) {
            throw new ArithmeticException("Denominator cannot be zero");
        }
        if (denominator.compareTo(BigInteger.ZERO) < 0) {
            numerator = numerator.negate();
            denominator = denominator.negate();
        }
        BigInteger gcd = numerator.gcd(numerator.gcd(denominator));
        this.numerator = numerator.divide(gcd);
        this.denominator = denominator.divide(gcd);
    }

    public Fraction(long numerator, long denominator) {
        this(BigInteger.valueOf(numerator), BigInteger.valueOf(denominator));
    }

    public static Fraction add(Fraction a, Fraction b) {
        BigInteger numerator = a.numerator.multiply(b.denominator).add(b.numerator.multiply(a.denominator));
        BigInteger denominator = a.denominator.multiply(b.denominator);
        return new Fraction(numerator, denominator);
    }

    public static Fraction sub(Fraction a, Fraction b) {
        BigInteger numerator = a.numerator.multiply(b.denominator).subtract(b.numerator.multiply(a.denominator));
        BigInteger denominator = a.denominator.multiply(b.denominator);
        return new Fraction(numerator, denominator);
    }

    public static Fraction mul(Fraction a, Fraction b) {
        BigInteger numerator = a.numerator.multiply(b.numerator);
        BigInteger denominator = a.denominator.multiply(b.denominator);
        return new Fraction(numerator, denominator);
    }

    public static Fraction div(Fraction a, Fraction b) {
        BigInteger numerator = a.numerator.multiply(b.denominator);
        BigInteger denominator = a.denominator.multiply(b.numerator);
        return new Fraction(numerator, denominator);
    }

    public static Fraction parseFraction(String str) throws NumberFormatException {
        String[] raw = str.split("/");
        if (raw.length == 2) {
            long numerator = Integer.parseInt(raw[0]);
            long denominator = Integer.parseInt(raw[1]);
            return new Fraction(numerator, denominator);
        }
        str = str.replace(",", ".");
        return fromDecimal(Double.parseDouble(str));
    }

    public static Fraction fromDecimal(double decimal) {
        String decimalString = Double.toString(decimal);
        int digitsDec = decimalString.length() - 1 - decimalString.indexOf('.');
        int denominator = 1;
        for (int i = 0; i < digitsDec; i++) {
            decimal *= 10;
            denominator *= 10;
        }
        int numerator = (int) Math.round(decimal);
        return new Fraction(numerator, denominator);
    }

    public boolean equals(Fraction f2) {
        BigInteger num1 = this.numerator;
        BigInteger den1 = this.denominator;
        BigInteger num2 = f2.numerator;
        BigInteger den2 = f2.denominator;

        return num1.equals(num2) && den1.equals(den2);
    }

    public int compareTo(Fraction other) {
        BigInteger diff = numerator.multiply(other.denominator).subtract(other.numerator.multiply(denominator));
        return diff.compareTo(BigInteger.ZERO);
    }

    public static Fraction ZERO = new Fraction(0, 1);
    public static Fraction ONE = new Fraction(1, 1);
    public static Fraction MINUS_ONE = new Fraction(-1, 1);

    @Override
    public String toString() {
        if (denominator.equals(BigInteger.ONE)) {
            return String.valueOf(this.numerator);
        }
        if (fractionType.equals(Task.FractionType.COMMON)) {
            return this.numerator + "/" + this.denominator;
        } else {
            DecimalFormat format = new DecimalFormat("#.####");
            return format.format(numerator.doubleValue() / denominator.doubleValue());
        }
    }

    @Override
    public Fraction clone() throws CloneNotSupportedException {
        return (Fraction) super.clone();
    }

    public double toDouble() throws ArithmeticException {
        return numerator.doubleValue() / denominator.doubleValue();
    }

    public static Fraction abs(Fraction fr) {
        return new Fraction(Math.abs(fr.numerator.intValue()), Math.abs(fr.denominator.intValue()));
    }
}
