package ru.ac.uniyar.tyumentsev.solver.controllers;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import ru.ac.uniyar.tyumentsev.solver.SolverApplication;
import ru.ac.uniyar.tyumentsev.solver.domain.Task;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class MenuController implements Initializable {
    private static final Logger logger = Logger.getLogger(SolverApplication.class.getName());
    public AnchorPane artificialBasis;
    public AnchorPane graphic;
    public AnchorPane selectedBasis;
    public AnchorPane conditionals;
    public Tab condTab;
    public Tab selTab;
    public Tab artTab;
    public Tab grTab;
    public TabPane tabs;

    @FXML
    private ConditionalsController conditionalsController;

    @FXML
    private SelectedBasisController selectedBasisController;

    @FXML
    private ArtificialBasisController artificialBasisController;

    @FXML GraphicController graphicController;

    private Task task;

    @FXML
    private void handleFileOpen() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Открыть файл");
        var f = new File("./tasks");
        if (f.isDirectory() && f.exists())
            fileChooser.setInitialDirectory(f);
        else
            fileChooser.setInitialDirectory(new File("./"));
        File file = fileChooser.showOpenDialog(null);
        if (file == null) {
            return;
        }
        try {
            this.task = Task.readJSON(file);
            deliverTaskToControllers();
        } catch (Exception ex) {
            logger.warning("Unable open a file");
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Невозможно открыть файл");
            a.setContentText("Ошибка открытия файла");
            a.show();
        }
    }

    @FXML
    private void handleFileSave() {
        try {
            conditionalsController.applyTask();
        } catch (NumberFormatException e) {
            return;
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранить в файл");
        fileChooser.setInitialFileName(".json");
        var f = new File("./tasks");
        if (f.isDirectory() && f.exists())
            fileChooser.setInitialDirectory(f);
        else
            fileChooser.setInitialDirectory(new File("./"));

        File file = fileChooser.showSaveDialog(null);
        if (file == null) {
            return;
        }
        try {
            task.saveJSON(file);
        } catch (Exception e) {
            logger.warning("Unable to save file");
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Невозможно сохранить файл");
            a.setContentText("Ошибка сохранения файла");
            a.show();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.task = new Task();
        deliverTaskToControllers();
    }

    public void deliverTaskToControllers() {
        conditionalsController.setTask(task);
        selectedBasisController.setTask(task);
        artificialBasisController.setTask(task);
        graphicController.setTask(task);
    }

    public void applyChanges(Event event) {
        Tab tab = (Tab) event.getSource();
        if (!tab.isSelected()) {
            try {
                conditionalsController.applyTask();
            } catch (NumberFormatException e) {
                tabs.getSelectionModel().clearSelection();
                tabs.getSelectionModel().select(condTab);
            }
        }
    }

    @FXML
    public void handleHelp() throws IOException {
        if (condTab.isSelected()) {
            showHelpWindow("conditionals-help.fxml");
        } else if (selTab.isSelected()) {
            showHelpWindow("selected-basis-help.fxml");
        } else if (artTab.isSelected()) {
            showHelpWindow("artificial-basis-help.fxml");
        } else if (grTab.isSelected()) {
            showHelpWindow("graphic-help.fxml");
        }
    }

    private static void showHelpWindow(String resourcePath) throws IOException {
        Alert infoDialog = new Alert(Alert.AlertType.INFORMATION);
        FXMLLoader loader = new FXMLLoader(SolverApplication.class.getResource(resourcePath));
        DialogPane dialogPane = loader.load();
        infoDialog.setDialogPane(dialogPane);
        infoDialog.setTitle("Помощь");
        infoDialog.showAndWait();
    }
}
