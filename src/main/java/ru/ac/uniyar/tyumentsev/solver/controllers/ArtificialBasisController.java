package ru.ac.uniyar.tyumentsev.solver.controllers;

import javafx.fxml.FXML;
import javafx.util.Pair;
import ru.ac.uniyar.tyumentsev.solver.algo.SimplexMethod;

public class ArtificialBasisController extends SimplexMethodController {

    public void onTaskUpdate() {
        super.onTaskUpdate();
        createSimplexTable(task.getnVars() + 2);
    }

    @FXML
    void onGetAnswer() {
        if (getnStep() == 0) {
            sm = new SimplexMethod(task);
            sm.performAuxiliaryTableFirstStep();
        }
        super.onGetAnswer();
    }

    @FXML
    void onStepForward() {
        if (getnStep() == 0) {
            sm = new SimplexMethod(task);
            sm.performAuxiliaryTableFirstStep();
            drawDataInTable();
            return;
        }
        super.onStepForward();
    }

    protected Pair<Integer, Integer> getSelectedReferenceItemCoords() {
        if (sm.getLastStep().isAuxiliaryLastTable()) {
            return new Pair<>(-1, -1);
        }
        return super.getSelectedReferenceItemCoords();
    }
}
