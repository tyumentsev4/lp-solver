package ru.ac.uniyar.tyumentsev.solver.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Matrix implements Cloneable {
    @JsonProperty
    private final ArrayList<ArrayList<Fraction>> data;

    public Matrix() {
        super();
        data = new ArrayList<>();
    }

    public Matrix(ArrayList<ArrayList<Fraction>> data) {
        this.data = data;
    }

    public Fraction getElementAt(int row, int col) {
        return data.get(row).get(col);
    }

    public void setElementAt(int row, int col, Fraction value) {
        data.get(row).set(col, value);
    }

    @JsonIgnore
    public int getRows() {
        return data.size();
    }

    @JsonIgnore
    public int getCols() {
        return data.get(0).size();
    }

    public ArrayList<ArrayList<Fraction>> getData() {
        return data;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getCols(); j++) {
                sb.append(this.getElementAt(i, j)).append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public static Matrix zeroMatrix(int m, int n) {
        ArrayList<ArrayList<Fraction>> arr = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            ArrayList<Fraction> row = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                row.add(new Fraction());
            }
            arr.add(row);
        }
        return new Matrix(arr);
    }

    @Override
    public Matrix clone() {
        try {
            super.clone();
            ArrayList<ArrayList<Fraction>> clonedData = new ArrayList<>();

            for (ArrayList<Fraction> row : data) {
                ArrayList<Fraction> clonedRow = new ArrayList<>();
                for (Fraction value : row) {
                    clonedRow.add(value.clone());
                }
                clonedData.add(clonedRow);
            }

            return new Matrix(clonedData);
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
