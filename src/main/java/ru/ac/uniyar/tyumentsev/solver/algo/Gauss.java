package ru.ac.uniyar.tyumentsev.solver.algo;

import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.SystemIsUnsolvable;
import ru.ac.uniyar.tyumentsev.solver.domain.Fraction;
import ru.ac.uniyar.tyumentsev.solver.domain.Matrix;

import java.util.ArrayList;
import java.util.Collections;

public class Gauss {
    Matrix matrix;
    ArrayList<Integer> col_indexes;

    public Gauss(Matrix matrix, ArrayList<Integer> col_indexes) {
        this.matrix = matrix;
        Collections.sort(col_indexes);
        this.col_indexes = col_indexes;
    }

    public void makeDiagonalMatrix() throws SystemIsUnsolvable {
        int m = matrix.getRows();
        int n = matrix.getCols();
        Fraction zero = new Fraction(0, 1);
        int j_sw = -1;
        int row_i = -1;
        Fraction temp;

        /* Прямой ход метода Гаусса */
        for (int k = 0; k < n; k++) { // Идем по столбцам
            if (col_indexes.contains(k)) {
                row_i++;
                Fraction d = matrix.getElementAt(row_i, k);
                if (!d.equals(zero))
                    for (int i = 0; i < n; i++) {
                        matrix.setElementAt(row_i, i, Fraction.div(matrix.getElementAt(row_i, i), d));
                    }
                for (int j = row_i + 1; j < m; j++) { // Идем по строкам ниже диагонали
                    if (matrix.getElementAt(row_i, k).equals(zero)) { // На диагонали 0
                        for (int i = row_i + 1; i < m; i++) { // Ищем строку, где ведущий элемент не 0
                            if (!(matrix.getElementAt(i, k).equals(zero))) {
                                j_sw = i;
                                break;
                            }
                        }
                        if (j_sw == -1) { // Если такой строки нет идем дальше по столбцу
                            continue;
                        }
                        for (int i = 0; i < n; i++) { // Меняем строки местами
                            temp = matrix.getElementAt(j - 1, i);
                            matrix.setElementAt(j - 1, i, matrix.getElementAt(j_sw, i));
                            matrix.setElementAt(j_sw, i, temp);
                        }
                    }
                    if (!(matrix.getElementAt(j, k).equals(zero))) {
                        Fraction diff = matrix.getElementAt(j, k);
                        for (int i = 0; i < n; i++) { //вычитаем строку
                            matrix.setElementAt(j, i, Fraction.sub(matrix.getElementAt(j, i), Fraction.mul(matrix.getElementAt(row_i, i), diff)));
                        }
                    }
                }
            }
        }

        /* Обратный ход */
        row_i = m;
        for (int k = n-1; k >= 0; k--) {
            if (col_indexes.contains(k)) {
                row_i--;
                for (int j = row_i - 1; j >= 0; j--) { // Идем по строкам выше диагонали
                    Fraction d = matrix.getElementAt(j, k);
                    for (int i = n-1; i >= 0; i--) { //вычитаем строку
                        matrix.setElementAt(j, i, Fraction.sub(matrix.getElementAt(j, i), Fraction.mul(matrix.getElementAt(row_i, i), d)));
                    }
                }
            }
        }

        // Проверка на разрешимость системы
        // Проверка базисных переменных
        for (int i = 0; i< m; i++) {
            if (matrix.getElementAt(i, col_indexes.get(i)).equals(zero)) {
                throw new SystemIsUnsolvable();
            }
        }

        // Проверка противоречия
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!matrix.getElementAt(i, j).equals(zero)) {
                    break;
                }
                else {
                    if (j == m-1) {
                        throw new SystemIsUnsolvable();
                    }
                }
            }
        }
    }

    public Matrix getMatrix() {
        return matrix;
    }

}
