package ru.ac.uniyar.tyumentsev.solver.controllers;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import ru.ac.uniyar.tyumentsev.solver.domain.Fraction;
import ru.ac.uniyar.tyumentsev.solver.domain.Matrix;
import ru.ac.uniyar.tyumentsev.solver.domain.Task;

import java.util.ArrayList;

public class ConditionalsController {

    ObservableList<Task.FractionType> fractionTypes = FXCollections.observableArrayList(Task.FractionType.values());
    ObservableList<Task.TaskType> taskTypes = FXCollections.observableArrayList(Task.TaskType.values());

    @FXML
    private ChoiceBox<Task.FractionType> fractionType;

    @FXML
    private Spinner<Integer> nRestr;

    @FXML
    private Spinner<Integer> nVars;

    @FXML
    private ChoiceBox<Task.TaskType> taskType;

    @FXML
    private GridPane restrInput;

    @FXML
    private GridPane fxInput;

    private Task task;

    private final ChangeListener<Number> nRestrListener = (observable, oldValue, newValue) -> onRestrSpinnerChanged();
    private final ChangeListener<Number> nVarsListener = (observable, oldValue, newValue) -> onVarsSpinnerChanged();

    public void setTask(Task task) {
        this.task = task;
        nRestr.valueProperty().removeListener(nRestrListener);
        nVars.valueProperty().removeListener(nVarsListener);
        nRestr.getValueFactory().setValue(task.getnRestr());
        nVars.getValueFactory().setValue(task.getnVars());
        fractionType.setValue(fractionTypes.get(0));
        fractionType.setItems(fractionTypes);

        taskType.setValue(taskTypes.get(0));
        taskType.setItems(taskTypes);
        nRestr.valueProperty().addListener((nRestrListener));
        nVars.valueProperty().addListener((nVarsListener));
        onTaskChange();
    }

    private void onRestrSpinnerChanged() {
        Integer nRestrVal = nRestr.getValue();
        Integer nVarsVal = nVars.getValue();
        if (nRestrVal > nVarsVal) {
            nVarsVal = nRestrVal;
            nVars.getValueFactory().setValue(nRestrVal);
        }
        task.fillTask(nRestrVal, nVarsVal);
        onTaskChange();
    }

    private void onVarsSpinnerChanged() {
        Integer nRestrVal = nRestr.getValue();
        Integer nVarsVal = nVars.getValue();
        if (nRestrVal > nVarsVal) {
            nRestrVal = nVarsVal;
            nRestr.getValueFactory().setValue(nRestrVal);
        }
        task.fillTask(nRestrVal, nVarsVal);
        onTaskChange();
    }

    private void onTaskChange() {
        updateFxTable();
        updateRestrTable();
        fractionType.setValue(task.getFractionType());
        taskType.setValue(task.getTaskType());
        taskType.setOnAction(event -> onTaskTypeChanged());
        fractionType.setOnAction(event -> onFractionTypeChanged());
        changeTaskTypeLabel();
    }

    /* Сборка таблицы целевой функции */
    private void updateFxTable() {
        fxInput.getChildren().clear();
        for (int i = 1; i <= task.getnVars() + 1; i++) {
            Label label;
            if (i == task.getnVars() + 1)
                label = new Label("b");
            else {
                label = new Label("x_" + i);
            }
            GridPane.setHalignment(label, HPos.CENTER);
            fxInput.add(label, i, 0);
            TextField textField = new TextField(task.getTargetFunction().get(i - 1).toString());
            textField.setMaxSize(80, 50);
            textField.setTooltip(new Tooltip("Пример: 1/2; -1; 2.5; 2,3"));
            fxInput.add(textField, i, 1);
        }
        Label label = new Label("f(x)");
        fxInput.add(label, 0, 1);
    }

    /* Сборка таблицы ограничений */
    private void updateRestrTable() {
        restrInput.getChildren().clear();
        for (int i = 1; i <= task.getnVars() + 1; i++) {
            Label label;
            if (i == task.getnVars() + 1)
                label = new Label("b");
            else {
                label = new Label("x_" + i);
            }
            GridPane.setHalignment(label, HPos.CENTER);
            restrInput.add(label, i, 0);
        }

        for (int i = 1; i <= task.getnRestr(); i++) {
            Label label = new Label("f(x)_" + i);
            restrInput.add(label, 0, i);
            for (int j = 1; j <= task.getnVars() + 1; j++) {
                TextField textField = new TextField(task.getRestrictions().getData().get(i - 1).get(j - 1).toString());
                textField.setMaxSize(80, 50);
                textField.setTooltip(new Tooltip("Пример: 1/2; -1; 2.5; 2,3"));
                restrInput.add(textField, j, i);
            }
        }
    }


    private void onTaskTypeChanged() {
        task.setTaskType(taskType.getValue());
        changeTaskTypeLabel();
    }

    private void onFractionTypeChanged() {
        task.setFractionType(fractionType.getValue());
    }

    private void changeTaskTypeLabel() {
        Label label = null;
        for (Node node : fxInput.getChildren()) {
            if (GridPane.getColumnIndex(node) == nVars.getValue() + 2 && GridPane.getRowIndex(node) == 1 && node instanceof Label) {
                label = (Label) node;
                break;
            }
        }
        if (label != null) {
            if (taskType.getValue() == Task.TaskType.TO_MAX) {
                label.setText("-> max");
            } else {
                label.setText("-> min");
            }
        } else {
            label = new Label();
            if (taskType.getValue() == Task.TaskType.TO_MAX) {
                label.setText("-> max");
                fxInput.add(label, nVars.getValue() + 2, 1);
            } else {
                label.setText("-> min");
                fxInput.add(label, nVars.getValue() + 2, 1);
            }
        }
    }

    void applyTask() throws NumberFormatException {
        try {
            task.updateTask(readTargetFunc(), readRestrictions(), taskType.getValue(), fractionType.getValue());
        } catch (NumberFormatException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Ошибка ввода");
            a.setContentText("Введите число. Пример: 1/2; -3/4; -2; 3.443; 2,5");
            a.show();
            updateFxTable();
            updateRestrTable();
            throw e;
        }
    }

    private ArrayList<Fraction> readTargetFunc() {
        ArrayList<Fraction> targetFunc = new ArrayList<>();
        for (Node node : fxInput.getChildren()) {
            if (node instanceof TextField) {
                targetFunc.add(Fraction.parseFraction(((TextField) node).getText()));
            }
        }
        return targetFunc;
    }

    private Matrix readRestrictions() {
        Matrix restrMatrix = Matrix.zeroMatrix(nRestr.getValue(), nVars.getValue()+1);
        restrInput.getChildren().add(new Label());
        for (int row = 1; row < restrInput.getRowCount(); row++) {
            for (int col = 0; col < restrInput.getColumnCount(); col++) {
                Node node = restrInput.getChildren().get(row * restrInput.getColumnCount() + col);
                if (node instanceof TextField) {
                    restrMatrix.setElementAt(row - 1, col, Fraction.parseFraction(((TextField) node).getText()));
                }
            }
        }
        return restrMatrix;
    }


    @FXML
    void onReset() {
        task.reset();
        setTask(task);
    }
}
