package ru.ac.uniyar.tyumentsev.solver.controllers;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.util.Pair;
import ru.ac.uniyar.tyumentsev.solver.SolverApplication;
import ru.ac.uniyar.tyumentsev.solver.algo.SimplexMethod;
import ru.ac.uniyar.tyumentsev.solver.algo.SimplexMethodStep;
import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.FunctionNotRestricted;
import ru.ac.uniyar.tyumentsev.solver.algo.exceptions.SystemIsUnsolvable;
import ru.ac.uniyar.tyumentsev.solver.domain.Matrix;
import ru.ac.uniyar.tyumentsev.solver.domain.Task;
import ru.ac.uniyar.tyumentsev.solver.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class SimplexMethodController {
    protected static final Logger logger = Logger.getLogger(SolverApplication.class.getName());
    @FXML
    private TableView<ArrayList<String>> simplexTable;

    @SuppressWarnings("rawtypes")
    protected ObservableList<TablePosition> selectedCells;
    private Pair<Integer, Integer> lastTableCoords;

    protected Task task;

    protected SimplexMethod sm;
    private boolean finish = false;

    protected int getnStep() {
        if (sm == null) return 0;
        return sm.getSimplexTableHistory().size();
    }

    public void setTask(Task task) {
        this.task = task;
        task.addListener(this::onTaskUpdate);
        onTaskUpdate();
    }

    public void onTaskUpdate() {
        sm = null;
        finish = false;
    }

    @FXML
    void onGetAnswer() {
        if (finish) return;
        if (getnStep() != 1 && sm.isSolved()) return;
        while (!sm.isSolved()) {
            try {
                sm.nextStep(-1, -1);
            } catch (FunctionNotRestricted e) {
                drawTables(sm.getSimplexTableHistory());
                drawAnswerStr("Функция не ограничена снизу");
                sm.markSolved();
                finish = true;
                return;
            } catch (SystemIsUnsolvable e) {
                drawTables(sm.getSimplexTableHistory());
                drawAnswerStr("Система несовместна");
                sm.markSolved();
                finish = true;
                return;
            }
        }
        drawDataInTable();
    }

    protected void drawDataInTable() {
        drawTables(sm.getSimplexTableHistory());
        if (sm.isSolved()) {
            logger.log(Level.INFO, "System solved");
            finish = true;
            drawAnswerStr(sm.getAnswerStr());
        }
    }

    @FXML
    void onStepForward() {
        if (finish) return;
        try {
            sm.isNextStep();
            Pair<Integer, Integer> selectedReferenceItemCoords = getSelectedReferenceItemCoords();
            sm.nextStep(selectedReferenceItemCoords.getKey(), selectedReferenceItemCoords.getValue());
        } catch (FunctionNotRestricted e) {
            logger.info("Функция не ограничена снизу");
            drawTables(sm.getSimplexTableHistory());
            drawAnswerStr("Функция не ограничена снизу");
            sm.markSolved();
            finish = true;
            return;
        } catch (SystemIsUnsolvable e) {
            logger.info("Система несовместна");
            drawTables(sm.getSimplexTableHistory());
            drawAnswerStr("Система несовместна");
            sm.markSolved();
            finish = true;
            return;
        }
        drawDataInTable();
    }

    private void drawAnswerStr(String answer) {
        TableColumn<ArrayList<String>, String> newColumn = new TableColumn<>("Ответ");
        newColumn.setCellValueFactory(cellData -> {
            ObservableList<String> rowData = FXCollections.observableArrayList(cellData.getValue());
            return Bindings.valueAt(rowData, simplexTable.getColumns().indexOf(newColumn));
        });
        newColumn.setReorderable(false);
        newColumn.setSortable(false);
        simplexTable.getColumns().add(newColumn);

        ArrayList<String> firstRow = simplexTable.getItems().get(0);
        firstRow.add(answer);
        Platform.runLater(() -> simplexTable.scrollTo(0));
    }

    @FXML
    void onStepBack() {
        logger.info("SimplexMethod step Back");
        finish = false;
        if (sm == null) return;
        if (sm.isSolved()) {
            simplexTable.getColumns().remove(simplexTable.getColumns().size() - 1);
        }
        if (getnStep() > 0) {
            sm.undoStep();
            drawTables(sm.getSimplexTableHistory());
        }
    }

    protected Pair<Integer, Integer> getSelectedReferenceItemCoords() {
        if (!selectedCells.isEmpty()) {
            TablePosition<?, ?> selectedCell = selectedCells.get(0);
            int rowIndex = selectedCell.getRow();
            int columnIndex = selectedCell.getColumn();
            return new Pair<>(rowIndex - lastTableCoords.getKey(), columnIndex - lastTableCoords.getValue());
        }
        return new Pair<>(-1, -1);
    }

    private void drawTables(ArrayList<SimplexMethodStep> simplexTableHistory) {
        simplexTable.getItems().clear();
        lastTableCoords = new Pair<>(1, 1);
        int aux_cnt = 0;
        int main_cnt = 0;
        for (int i = 0; i < simplexTableHistory.size(); i++) {
            if (i > 0) {
                lastTableCoords = new Pair<>(lastTableCoords.getKey() + simplexTableHistory.get(i - 1).getSimplexTable().getRows() + 2, lastTableCoords.getValue());
            }
            Matrix st = simplexTableHistory.get(i).getSimplexTable();
            ArrayList<Integer> basisNums = simplexTableHistory.get(i).getBasisVarNumbers();
            ArrayList<Integer> freeNums = simplexTableHistory.get(i).getFreeVarNumbers();
            for (int j = 0; j < st.getRows() + 1; j++) {
                ArrayList<String> list = new ArrayList<>(Collections.nCopies(simplexTable.getColumns().size(), ""));
                for (int k = 0; k < st.getCols() + 1; k++) {
                    if (j == 0 && k == 0) {
                        if (simplexTableHistory.get(i).isAuxiliaryTable()) {
                            list.set(k, "ẋ" + StringUtils.superscript("(" + aux_cnt + ")"));
                            aux_cnt++;
                        } else {
                            list.set(k, "x" + StringUtils.superscript("(" + main_cnt + ")"));
                            main_cnt++;
                        }
                        continue;
                    }
                    if (j == 0 && k < st.getCols()) {
                        list.set(k, "x" + StringUtils.subscript(freeNums.get(k - 1).toString()));
                        continue;
                    }
                    if (k == 0 && j <= st.getRows()) {
                        if (j == st.getRows()) {
                            list.set(k, "f(x)");
                        } else {
                            list.set(k, "x" + StringUtils.subscript(basisNums.get(j - 1).toString()));
                        }
                        continue;
                    }
                    if (k > 0 && j > 0 && k <= st.getCols() && j <= st.getRows()) {
                        list.set(k, st.getElementAt(j - 1, k - 1).toString());
                    }
                }
                simplexTable.getItems().add(list);
            }
            simplexTable.getItems().add(new ArrayList<>(Collections.nCopies(simplexTable.getColumns().size(), "")));
        }
        highlightRefCells(simplexTable, simplexTableHistory);
        Platform.runLater(() -> simplexTable.scrollTo(simplexTable.getItems().size()-1));
        simplexTable.refresh();
    }

    @SuppressWarnings("unchecked")
    private void highlightRefCells(TableView<ArrayList<String>> simplexTable, ArrayList<SimplexMethodStep> simplexMethodSteps) {
        int cur_row = 0;
        HashMap<Integer, ArrayList<Integer>> candidatesRefCells = new HashMap<>(); // отображение: колонка -> список строк
        HashMap<Integer, ArrayList<Integer>> selectedRefCells = new HashMap<>();
        for (int i = 0; i < simplexMethodSteps.size(); i++) {
            SimplexMethodStep simplexMethodStep = simplexMethodSteps.get(i);
            ArrayList<Pair<Integer, Integer>> allowed_pos = simplexMethodStep.getAllowedRefIndexes();
            Matrix table = simplexMethodStep.getSimplexTable();

            for (int col_j = 1, col_m = 0; col_m < table.getCols() - 1; col_j++, col_m++) {
                for (int row_i = cur_row + 1, row_m = 0; row_m < table.getRows() - 1; row_i++, row_m++) {
                    // Для последней таблицы отмечаем кандидатов на опорный элемент
                    if (i == simplexMethodSteps.size() - 1) {
                        Pair<Integer, Integer> cellPos = new Pair<>(row_m, col_m);
                        if (allowed_pos.contains(cellPos)) {
                            candidatesRefCells.computeIfAbsent(col_j, k -> new ArrayList<>());
                            candidatesRefCells.get(col_j).add(row_i);
                            logger.fine("Ref candidate coordinate row: " + row_i + " col: " + col_j);
                        }
                    } else {
                        Pair<Integer, Integer> selectedRefCellPos = simplexMethodStep.getSelectedRefItemCoords();
                        if (selectedRefCellPos == null) continue;
                        if (selectedRefCellPos.getKey() == row_m && selectedRefCellPos.getValue() == col_m) {
                            selectedRefCells.computeIfAbsent(col_j, k -> new ArrayList<>());
                            selectedRefCells.get(col_j).add(row_i);
                            logger.fine("Selected ref coordinate row: " + row_i + " col: " + col_j);
                        }
                    }
                }
            }
            cur_row += table.getRows() + 2;
        }
        for (int col_i = 0; col_i < simplexTable.getColumns().size(); col_i++) {
            TableColumn<ArrayList<String>, String> column = (TableColumn<ArrayList<String>, String>) simplexTable.getColumns().get(col_i);
            int finalCol_i = col_i;
            column.setCellFactory(col -> new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (selectedRefCells.get(finalCol_i) != null && selectedRefCells.get(finalCol_i).contains(getIndex())) {
                        getStyleClass().add("selectedRefCell");
                    } else {
                        getStyleClass().remove("selectedRefCell");
                    }
                    if (candidatesRefCells.get(finalCol_i) != null && candidatesRefCells.get(finalCol_i).contains(getIndex())) {
                        getStyleClass().add("refCellCandidate");
                        setTooltip(new Tooltip("Может быть опорным элементом."));
                        setDisable(false);
                    } else {
                        getStyleClass().remove("refCellCandidate");
                        setDisable(true);
                    }
                    setText(empty ? null : item);
                }
            });
        }
    }

    protected void createSimplexTable(Integer nCol) {
        simplexTable.getItems().clear();
        simplexTable.getColumns().clear();
        simplexTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        simplexTable.getSelectionModel().setCellSelectionEnabled(true);
        for (int i = 0; i < nCol; i++) {
            TableColumn<ArrayList<String>, String> tableColumn = new TableColumn<>();
            int finalI = i;
            tableColumn.setCellValueFactory(param -> new ReadOnlyStringWrapper(param.getValue().get(finalI)));
            tableColumn.setReorderable(false);
            tableColumn.setSortable(false);
            simplexTable.getColumns().add(tableColumn);
        }
        selectedCells = simplexTable.getSelectionModel().getSelectedCells();
    }

    protected static class CellNotSelected extends Exception {
    }
}
